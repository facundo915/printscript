package interpreter;

import ast.Expression;

import java.util.stream.Stream;

public interface Interpreter {
    public void interpret(Stream<Expression> expressionStream);
}
