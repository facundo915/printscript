package interpreter.impl;

import ast.Expression;

import java.util.HashMap;
import java.util.Map;

public class ContextHandler {
    private Map<String,Context> fileContext = new HashMap<>();

    public Context get(Expression expression) {
        Context ret = fileContext.get(expression.getLocation().getFile());
        if(ret == null){
            ret = new Context();
        }
        fileContext.put(expression.getLocation().getFile(),ret);
        return ret;
    }
}
