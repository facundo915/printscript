package reader;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;
import line.Line;

public interface Reader {
    Stream<Line> readFile(File f) throws IOException;

}
