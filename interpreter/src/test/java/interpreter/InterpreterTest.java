package interpreter;

import ast.Expression;
import interpreter.impl.InterpreterImpl;
import lexer.Lexer;
import lexer.impl.LexerImpl;
import line.Line;
import org.junit.jupiter.api.*;
import parser.impl.ParserImpl;
import tokens.Token;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class InterpreterTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void helloWorld() {
        Line test = new Line("println(\"Hello,\" + \" World\");", 1, "TEST");
        Lexer lexer = new LexerImpl();
        Stream<Token> lex = lexer.lex(Stream.of(test));
        ParserImpl parser = new ParserImpl();
        Stream<Expression> parse = parser.parse(lex);
        InterpreterImpl interpreter = new InterpreterImpl();
        interpreter.interpret(parse);
        assertEquals("Hello, World\n", outContent.toString());
    }

    @Test
    void testAssignment() {
        Line test2 = new Line("let testVariable2:number = 5;", 2, "TEST");
        Line test3 = new Line("println(testVariable2);", 3, "TEST");
        Lexer lexer = new LexerImpl();
        Stream<Token> lex = lexer.lex(Stream.of(test2, test3));
        ParserImpl parser = new ParserImpl();
        Stream<Expression> parse = parser.parse(lex);
        InterpreterImpl interpreter = new InterpreterImpl();
        interpreter.interpret(parse);
        assertEquals("5.0\n", outContent.toString());
    }

    @Test
    void testAssignmentWithBinaryOperation() {
        Line test4 = new Line("let testVariable3:number = 5 + 3;", 4, "TEST");
        Line test5 = new Line("println(testVariable3);", 5, "TEST");
        Lexer lexer = new LexerImpl();
        Stream<Token> lex = lexer.lex(Stream.of(test4, test5));
        ParserImpl parser = new ParserImpl();
        Stream<Expression> parse = parser.parse(lex);
        InterpreterImpl interpreter = new InterpreterImpl();
        interpreter.interpret(parse);
        assertEquals("8.0\n", outContent.toString());
    }

    @Test
    void testConditionPrint() {
        Line test6 = new Line("let booleanVar:boolean = false;", 6, "TEST");
        Line test7 = new Line("if(booleanVar){ println(\"IT WAS TRUE\"); } else { println(\"IT WAS NOT TRUE\"); }", 7, "TEST");
        Lexer lexer = new LexerImpl();
        Stream<Token> lex = lexer.lex(Stream.of(test6, test7));
        ParserImpl parser = new ParserImpl();
        Stream<Expression> parse = parser.parse(lex);
        InterpreterImpl interpreter = new InterpreterImpl();
        interpreter.interpret(parse);
        assertEquals("IT WAS NOT TRUE\n", outContent.toString());
    }

    //const a:number = 5 + 1;
//println(a);
//const bool:boolean = a > 1;
    @Test
    void te() {
        Line test6 = new Line("const a:number = 5 + 1;", 6, "TEST");
        Line test7 = new Line("const bool:boolean = a > 1;", 7, "TEST");
        Lexer lexer = new LexerImpl();
        Stream<Token> lex = lexer.lex(Stream.of(test6, test7));
        ParserImpl parser = new ParserImpl();
        Stream<Expression> parse = parser.parse(lex);
        InterpreterImpl interpreter = new InterpreterImpl();
        interpreter.interpret(parse);
    }
}