package ast.operations;

import ast.ASTVisitor;
import ast.Expression;
import ast.definitions.Initializer;

public class Assignment extends Expression{
    private String identifier;
    private Initializer value;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Initializer getValue() {
        return value;
    }

    public void setValue(Initializer value) {
        this.value = value;
    }
}
