package interpreter.impl;

import ast.Expression;
import interpreter.Interpreter;

import java.util.stream.Stream;

public class InterpreterImpl implements Interpreter {
    public void interpret(Stream<Expression> expressionStream){
        InterpreterVisitor interpreter = new InterpreterVisitor();
        expressionStream.forEachOrdered(expression -> expression.accept(interpreter));
    }
}
