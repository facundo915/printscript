package cli;

import ast.Expression;
import interpreter.Interpreter;
import interpreter.impl.InterpreterImpl;
import lexer.Lexer;
import lexer.impl.LexerImpl;
import line.Line;
import parser.Parser;
import parser.impl.ParserImpl;
import reader.Reader;
import reader.impl.ReaderImpl;
import tokens.Token;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            usage();
            return;
        }
        Reader reader = new ReaderImpl();
        Lexer lexer = new LexerImpl();
        Parser parser = new ParserImpl();
        Interpreter interpreter = new InterpreterImpl();
        System.out.println("Reading file");
        Stream<Line> lineStream = reader.readFile(new File(args[0]));
        System.out.println("Lexing...");
        Stream<Token> lex = lexer.lex(lineStream);
        System.out.println("Parsing...");
        Stream<Expression> parse = parser.parse(lex);
        if (args.length > 1 && args[1].equals("validate"))
            return;
        System.out.println("Interpreting...");
        interpreter.interpret(parse);

    }

    private static void usage() {
        System.out.println("PrintScript Interpreter!");
        System.out.println("Usage: ");
        System.out.println("java -jar cli.jar file [MODE]");
        System.out.println("Mode can be: interpret or validate");
        System.out.println("Default is interpret");
    }
}
