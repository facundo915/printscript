package parser;

import ast.Expression;
import tokens.Token;

import java.util.stream.Stream;

public interface Parser {
    Stream<Expression> parse(Stream<Token> tokens);
}
