package locatable;

public interface Locatable {
    Location getLocation();
}
