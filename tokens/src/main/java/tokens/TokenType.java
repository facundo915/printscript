package tokens;

import java.util.regex.Pattern;

public enum TokenType {
    ADDITION("\\+"),
    SUBTRACTION("-"),
    MULTIPLICATION("\\*"),
    DIVISION("\\/"),
    EQUAL("=="),
    GREATER_EQUAL(">="),
    LESS_EQUAL("<="),
    LESS("<"),
    GREATER(">"),

    LPAREN("\\("),
    RPAREN("\\)"),
    LBRACE("\\{"),
    RBRACE("\\}"),

    ASSIGN("="),
    SEMICOLON(";"),
    COLON(":"),
    COMMA(","),

    LET("let"),
    CONST("const"),
    TYPE_NUMBER("\\bnumber\\b"),
    TYPE_STRING("\\bstring\\b"),
    TYPE_BOOLEAN("\\bboolean\\b"),
    IF("if"),
    ELSE("else"),
    PRINT("println"),
    TRUE("true"),
    FALSE("false"),

    STRING ("\"[^\"]+\""), //String literal
    REAL ("(\\d+)\\.\\d+"),
    INTEGER("\\d+"),
    IDENTIFIER ("\\w+");

    private final Pattern pattern;

    TokenType(String regex) {
        pattern = Pattern.compile("^" + regex);
    }

    public Pattern getPattern() {
        return pattern;
    }
}
