package ast.operations;

import ast.ASTVisitor;
import ast.Expression;
import ast.definitions.Initializer;
import ast.definitions.Type;

import java.util.Optional;

public class Declaration extends Expression {

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    private boolean constant;
    private String identifier;
    private Type type;
    private Initializer initializer; //Binary or constant

    public boolean isConstant() {
        return constant;
    }

    public void setConstant(boolean constant) {
        this.constant = constant;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Optional<Initializer> getInitializer() {
        return Optional.ofNullable(initializer);
    }

    public void setInitializer(Initializer initializer) {
        this.initializer = initializer;
    }
}
