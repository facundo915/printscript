package lexer.impl;

import line.Line;
import org.junit.jupiter.api.Test;
import tokens.Token;
import tokens.TokenType;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LexerImplTest {

    @Test
    void lex() {
        LexerImpl lexer = new LexerImpl();
        Line test = new Line("let testVariable:number = 1.5;",1,"TEST");
        Line test2 = new Line("let testVariable2:number = 5;",2,"TEST");
        List<Token> lex = lexer.lex(Stream.of(test,test2)).collect(Collectors.toList());
        Token assign = lex.get(0);

        assertEquals("let", assign.getContent());
        assertEquals(TokenType.LET,assign.getTokenType());

        Token variableName = lex.get(1);
        assertEquals("testVariable", variableName.getContent());
        assertEquals(TokenType.IDENTIFIER,variableName.getTokenType());

        Token semi = lex.get(2);
        assertEquals(":", semi.getContent());
        assertEquals(TokenType.COLON,semi.getTokenType());

        Token number = lex.get(3);
        assertEquals("number", number.getContent());
        assertEquals(TokenType.TYPE_NUMBER,number.getTokenType());

        Token eq = lex.get(4);
        assertEquals("=", eq.getContent());
        assertEquals(TokenType.ASSIGN,eq.getTokenType());

        Token numb1 = lex.get(5);
        assertEquals("1.5", numb1.getContent());
        assertEquals(TokenType.REAL,numb1.getTokenType());
    }
}