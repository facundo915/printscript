package ast.definitions;

import java.util.Map;

public class BooleanConstant implements Initializer {
    private Boolean value;

    public BooleanConstant(String content) {
        value = Boolean.parseBoolean(content);
    }

    public Boolean getValue(Map<String, Variable> context) {
        return value;
    }

    @Override
    public Type getType(Map<String, Variable> context) {
        return Type.BOOLEAN;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

}
