package ast.operations;

import ast.ASTVisitor;
import ast.Expression;
import ast.definitions.Initializer;

public class Print extends Expression {
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
    private String identifier;
    private Initializer expression;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Initializer getExpression() {
        return expression;
    }

    public void setExpression(Initializer expression) {
        this.expression = expression;
    }
}
