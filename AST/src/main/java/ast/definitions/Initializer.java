package ast.definitions;

import java.util.Map;

public interface Initializer {
    Object getValue(Map<String, Variable> context);
    Type getType(Map<String, Variable> context);
}
