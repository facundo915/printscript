package parser.impl;

import ast.definitions.*;
import parser.exception.*;
import parser.Parser;
import parser.util.UndoIterator;
import ast.Expression;
import ast.operations.*;
import tokens.Token;
import tokens.TokenType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static tokens.TokenType.*;

public class ParserImpl implements Parser {
    @Override
    public Stream<Expression> parse(Stream<Token> tokens) {
        UndoIterator<Token> iterator = new UndoIterator<>(tokens.iterator());
        List<Expression> expressions = new ArrayList<>();
        Expression next;
        while (iterator.hasNext() && (next = parse(iterator.next(), iterator)) != null) {
            expressions.add(next);
        }
        return expressions.stream();
    }

    private Expression parse(Token token, UndoIterator<Token> iterator) {
        if (iterator.hasNext()) {
            switch (token.getTokenType()) {
                case IF:
                    return parseIf(token,iterator);
                case LET:
                    return parseDeclaration(token,iterator, false);
                case CONST:
                    return parseDeclaration(token,iterator, true);
                case LBRACE:
                    return parseCompound(token,iterator);
                case PRINT:
                    return parsePrint(token,iterator);
                case IDENTIFIER:
                    return parseAssignment(token,iterator);
            }
        }
        return null;
    }

    private Expression parsePrint(Token first,UndoIterator<Token> iterator) {
        Print print = new Print();
        print.setLocation(first.getLocation());
        consume(iterator,LPAREN);
        if(!iterator.hasNext())
            throw new UnexpectedEndOfFile();

        Token next = iterator.next();
        if(next.getTokenType() == IDENTIFIER){
            print.setIdentifier(next.getContent());
        }else {
            iterator.undo();
            print.setExpression(consumeConstantExpression(iterator));
        }
        consume(iterator,RPAREN);
        consume(iterator,SEMICOLON);
        return print;
    }

    private Expression parseAssignment(Token first,UndoIterator<Token> iterator) {
        Assignment assignment = new Assignment();
        assignment.setLocation(first.getLocation());
        assignment.setIdentifier(consume(iterator, IDENTIFIER).getContent());
        consume(iterator,ASSIGN);
        assignment.setValue(consumeConstantExpression(iterator));
        consume(iterator,SEMICOLON);
        return assignment;
    }

    private Expression parseDeclaration(Token first,UndoIterator<Token> iterator, boolean isConstant) {
        Declaration declaration = new Declaration();
        declaration.setLocation(first.getLocation());
        declaration.setConstant(isConstant);

        declaration.setIdentifier(consume(iterator, IDENTIFIER).getContent());

        consume(iterator, TokenType.COLON);

        declaration.setType(consumeType(iterator));

        //consume(iterator, ASSIGN);
        if(!iterator.hasNext()){
            throw new UnexpectedEndOfFile();
        }
        Token next = iterator.next();
        if(next.getTokenType() == SEMICOLON) {
            if(isConstant){
                throw new UninitializedConstantException();
            }
            return declaration;
        }
        else if(next.getTokenType() != ASSIGN)
            throw new UnexpectedTokenException(ASSIGN, next.getTokenType());

        declaration.setInitializer(consumeConstantExpression(iterator));

        consume(iterator,SEMICOLON);

        return declaration;
    }

    private Expression parseIf(Token first,UndoIterator<Token> iterator) {
        If parsed = new If();
        parsed.setLocation(first.getLocation());
        consume(iterator, TokenType.LPAREN);

        Token identifier = consume(iterator, TokenType.IDENTIFIER);

        parsed.setConditionIdentifier(identifier.getContent());

        consume(iterator, TokenType.RPAREN);
        Token lBrace = consume(iterator, TokenType.LBRACE);

        parsed.setRun(parseCompound(lBrace,iterator));

        if(!iterator.hasNext())
            return parsed;

        Token next = iterator.next();
        if (next.getTokenType() == TokenType.ELSE) {
            lBrace = consume(iterator, TokenType.LBRACE);
            parsed.setElseBlock(parseCompound(lBrace,iterator));
        } else {
            iterator.undo();
        }
        return parsed;
    }

    private Expression parseCompound(Token first,UndoIterator<Token> iterator) {
        CompoundExpression compoundExpression = new CompoundExpression();
        compoundExpression.setLocation(first.getLocation());
        if (!iterator.hasNext())
            throw new UnexpectedEndOfFile();
        Token t = iterator.next();
        while (t.getTokenType() != TokenType.RBRACE) {
            Expression next = parse(t, iterator);
            compoundExpression.addExpression(next);
            t = iterator.next();
        }
        return compoundExpression;
    }

    private Token consume(UndoIterator<Token> iterator, TokenType type) {
        if (!iterator.hasNext())
            throw new UnexpectedTokenException(type);
        Token t = iterator.next();
        if (t.getTokenType() != type)
            throw new UnexpectedTokenException(type, t.getTokenType());
        return t;
    }

    private Type consumeType(UndoIterator<Token> iterator) {
        if (!iterator.hasNext())
            throw new UnexpectedEndOfFile();
        Token typeToken = iterator.next();
        Type parsedType;
        switch (typeToken.getTokenType()) {
            case TYPE_NUMBER:
                parsedType = Type.NUMBER;
                break;
            case TYPE_BOOLEAN:
                parsedType = Type.BOOLEAN;
                break;
            case TYPE_STRING:
                parsedType = Type.STRING;
                break;
            default:
                throw new InvalidTypeException();
        }
        return parsedType;
    }

    private Initializer consumeConstant(UndoIterator<Token> iterator) {
        if (!iterator.hasNext()) {
            throw new UnexpectedEndOfFile();
        }
        Token t = iterator.next();
        Initializer res;
        switch (t.getTokenType()) {
            case INTEGER:
            case REAL:
                res = new NumberConstant(t.getContent());
                break;
            case STRING:
                res = new StringConstant(t.getContent());
                break;
            case FALSE:
            case TRUE:
                res = new BooleanConstant(t.getContent());
                break;
            case IDENTIFIER:
                res = new VariableConstant(t.getContent());
                break;
            default:
                throw new InvalidTypeException();
        }
        return res;
    }

    //Get either a constant or a binaryOperation
    private Initializer consumeConstantExpression(UndoIterator<Token> iterator) {
        Initializer constant = consumeConstant(iterator);

        if (!iterator.hasNext())
            throw new UnexpectedEndOfFile();
        Token next = iterator.next();
        if (next.getTokenType() == SEMICOLON || next.getTokenType() == RPAREN) {
            iterator.undo();
            return constant;
        }
        Operation operation = getOperation(next);
        BinaryOperation binaryOperation = new BinaryOperation();
        binaryOperation.setLeft(constant);
        binaryOperation.setOperand(operation);
        binaryOperation.setRight(consumeConstant(iterator));
        return binaryOperation;
    }


    private Operation getOperation(Token next) {
        switch (next.getTokenType()) {
            case ADDITION:
                return Operation.ADDITION;
            case SUBTRACTION:
                return Operation.SUBTRACTION;
            case MULTIPLICATION:
                return Operation.MULTIPLICATION;
            case DIVISION:
                return Operation.DIVISION;
            case LESS:
                return Operation.LESS_THAN;
            case LESS_EQUAL:
                return Operation.LESS_OR_EQUAL;
            case EQUAL:
                return Operation.EQUALS;
            case GREATER:
                return Operation.GREATER;
            case GREATER_EQUAL:
                return Operation.GREATER_OR_EQUAL;
        }
        throw new UnexpectedOperationException();
    }
}

