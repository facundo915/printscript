package ast;

import ast.operations.*;

public interface ASTVisitor {

    void visit(Declaration declaration);
    void visit(Print print);
    void visit(If anIf);
    void visit(CompoundExpression compoundExpression);
    void visit(Assignment assignment);
}
