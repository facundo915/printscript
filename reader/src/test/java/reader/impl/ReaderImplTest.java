package reader.impl;

import line.Line;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ReaderImplTest {

    private static String normalFilePath = "/tmp/normal.ps";
    private static String importFilePath = "/tmp/imports.ps";

    @BeforeAll
    public static void createTestFiles() throws IOException {
        FileWriter normal = new FileWriter(new File(normalFilePath),false);
        normal.write("TEST 1\n");
        normal.append("This is the second line in normal file\n");
        normal.append("This is the third line in normal file\n");
        normal.close();
        FileWriter imports = new FileWriter(new File(importFilePath));
        imports.write("TEST 2\n");
        imports.append("import \'normal.ps\'\n");
        imports.append("Third line in import file\n");
        imports.append("Fourth line in import file\n");
        imports.close();
    }

    @Test
    public void testNormalFile() throws IOException {
        ReaderImpl reader = new ReaderImpl();
        Stream<Line> lineStream = reader.readFile(new File(normalFilePath));
        List<Line> collect = lineStream.collect(Collectors.toList());
        assertEquals("TEST 1", collect.get(0).getContents());
        assertEquals("This is the second line in normal file", collect.get(1).getContents());
        assertEquals(1, collect.get(0).getLine());
        assertEquals(normalFilePath,collect.get(0).getFile());
        assertEquals(2,collect.get(1).getLine());
    }

    @Test
    public void testImportsFile() throws IOException {
        ReaderImpl reader = new ReaderImpl();
        Stream<Line> lineStream = reader.readFile(new File(importFilePath));
        List<Line> collect = lineStream.collect(Collectors.toList());
        //Test the first line is loaded, its line index is right and the file is the right one
        assertEquals("TEST 2", collect.get(0).getContents());
        assertEquals(1, collect.get(0).getLine());
        assertEquals(importFilePath,collect.get(0).getFile());

        //As the import is the second line, test that the second line returned is the first line of the imported file
        //Also check that the line has the correct file set and the correct order for that file (first line of "normal" file)
        assertEquals("TEST 1", collect.get(1).getContents());
        assertEquals(1, collect.get(1).getLine());
        assertEquals(normalFilePath,collect.get(1).getFile());

        //Test that after the imports, the original file's lines have the correct order for their file (import)
        assertEquals("Third line in import file", collect.get(4).getContents());
        assertEquals(3,collect.get(4).getLine());
        assertEquals(importFilePath,collect.get(4).getFile());
    }
}