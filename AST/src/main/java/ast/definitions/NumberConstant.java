package ast.definitions;

import java.util.Map;

public class NumberConstant implements Initializer {
    public NumberConstant(String content) {
        value = Double.parseDouble(content);
    }

    private Double value;

    public Double getValue(Map<String, Variable> context) {
        return value;
    }

    @Override
    public Type getType(Map<String, Variable> context) {
        return Type.NUMBER;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
