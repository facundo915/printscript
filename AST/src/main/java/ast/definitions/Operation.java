package ast.definitions;

import java.util.function.BiFunction;

public enum Operation {
    ADDITION((l,r) -> l+r),
    SUBTRACTION((l,r) -> l-r),
    MULTIPLICATION((l,r) -> l*r),
    EQUALS((l,r) -> l.equals(r)),
    GREATER((l,r) -> l > r),
    GREATER_OR_EQUAL((l,r) -> l >= r),
    LESS_THAN((l,r) -> l < r),
    DIVISION((l,r) -> l/r),
    LESS_OR_EQUAL((l,r) -> l <= r);

    BiFunction<Double,Double,Object> doubleFunction;

    Operation(BiFunction<Double,Double,Object> function){
        this.doubleFunction = function;
    }

    public BiFunction<Double, Double, Object> getDoubleFunction() {
        return doubleFunction;
    }
}
