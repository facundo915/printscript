package ast;

import locatable.Locatable;
import locatable.Location;

public abstract class Expression implements Locatable {
    private Location location;

   public abstract void accept(ASTVisitor visitor);

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
