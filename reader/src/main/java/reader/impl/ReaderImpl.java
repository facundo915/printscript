package reader.impl;

import line.Line;
import reader.Reader;

import java.io.*;
import java.nio.file.Files;
import java.util.stream.Stream;

public class ReaderImpl implements Reader {
    @Override
    public Stream<Line> readFile(File f) throws IOException {
        var fileLineRef = new Object() {
            int counter = 1;
        };
        Stream<String> lines = Files.lines(f.toPath());
        return lines.flatMap(line -> {
            if (line.trim().startsWith("import")) {
                String filename = line.split("import")[1].split("\'")[1].split("\'")[0];
                fileLineRef.counter++;
                try {
                    return readFile(new File(f.toPath().getParent().resolve(filename).toString()));
                } catch (IOException e) {
                    //TODO: Be less awful
                    e.printStackTrace();
                }
            }
            return Stream.of(new Line(line, fileLineRef.counter++, f.getAbsolutePath()));
        });
    }
}
