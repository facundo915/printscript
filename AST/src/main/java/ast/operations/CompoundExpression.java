package ast.operations;

import ast.ASTVisitor;
import ast.Expression;

import java.util.ArrayList;
import java.util.List;

public class CompoundExpression extends Expression {
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    private List<Expression> expressions;

    public List<Expression> getExpressions() {
        return expressions;
    }

    public void setExpressions(List<Expression> expressions) {
        this.expressions = expressions;
    }

    public void addExpression(Expression next) {
        if(expressions == null){
            expressions = new ArrayList<>();
        }
        expressions.add(next);
    }
}
