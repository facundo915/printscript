package interpreter.exception;

import ast.definitions.Type;

public class MismatchedTypeException extends RuntimeException {
    public MismatchedTypeException(Type expected, Type got) {
        super("Invalid type found: expected " + expected.name() + " but got " + got.name());
    }
}
