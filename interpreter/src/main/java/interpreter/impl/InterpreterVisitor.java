package interpreter.impl;

import ast.ASTVisitor;
import ast.definitions.*;
import ast.operations.*;
import interpreter.exception.MismatchedTypeException;

public class InterpreterVisitor implements ASTVisitor {
    private ContextHandler context = new ContextHandler();

    @Override
    public void visit(Declaration declaration) {
        Context ctx = context.get(declaration);
        ctx.declare(declaration);
    }

    @Override
    public void visit(Print print) {
        Context ctx = context.get(print);
        Initializer expression = print.getExpression();
        if(expression != null){
            System.out.println(expression.getValue(ctx.getContextAsMap()));
        }else{
            System.out.println(ctx.get(print.getIdentifier()).getValue());
        }
    }

    @Override
    public void visit(If anIf) {
        Context ctx = context.get(anIf);
        Variable variable = ctx.get(anIf.getConditionIdentifier());
        if(variable.getType() != Type.BOOLEAN)
            throw new MismatchedTypeException(Type.BOOLEAN, variable.getType());
        if((boolean) variable.getValue()){
            anIf.getRun().accept(this);
        }else{
            anIf.getElseBlock()
                    .ifPresent(expression -> expression.accept(this));
        }
    }

    @Override
    public void visit(CompoundExpression compoundExpression) {
        compoundExpression.getExpressions().forEach(expression -> expression.accept(this));
    }

    @Override
    public void visit(Assignment assignment) {
        Context ctx = context.get(assignment);
        ctx.assign(assignment.getIdentifier(),assignment.getValue());

    }
}
