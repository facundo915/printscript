package locatable;

public class Location {
    private String file;
    private Integer line;
    private Integer column;

    public Location(String file, Integer line, Integer column) {
        this.file = file;
        this.line = line;
        this.column = column;
    }

    public Location() {

    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }
}
