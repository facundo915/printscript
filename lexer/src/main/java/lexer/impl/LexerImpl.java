package lexer.impl;

import lexer.Lexer;
import line.Line;
import locatable.Location;
import tokens.Token;
import tokens.TokenType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.stream.Stream;

public class LexerImpl implements Lexer {
    @Override
    public Stream<Token> lex(Stream<Line> lines) {
        return lines.flatMap(this::tokenize);
    }

    private Stream<Token> tokenize(Line line) {
        return List.of(line.getContents())
                .stream()
                .flatMap(word -> tokenize(word.trim(), line.getFile(), line.getLine(),line.getContents()));
    }

    private Stream<Token> tokenize(String input, String file, Integer line,String ogLine){
        return tokenizes(input,file,line,ogLine);
    }

    private Stream<Token> tokenizes(String input, String file, Integer line, String ogLine){
        for(TokenType t : TokenType.values()){
            Matcher m = t.getPattern().matcher(input);
            if (m.find()) {
                Token r = new Token();
                r.setTokenType(t);
                if(t == TokenType.STRING) // Cut off "
                    r.setContent(input.substring(m.start() + 1,m.end() - 1));
                else
                    r.setContent(m.group());
                r.setLocation(new Location(file,line,ogLine.indexOf(input)));
                return Stream.concat(Stream.of(r),tokenizes(input.substring(m.end()).trim(),file,line, ogLine));
            }
        }
        return Stream.empty();
    }

}
