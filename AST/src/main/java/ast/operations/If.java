package ast.operations;

import ast.ASTVisitor;
import ast.Expression;

import java.util.Optional;

public class If extends Expression {
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
    private String conditionIdentifier;
    private Expression run;
    private Expression elseBlock;

    public String getConditionIdentifier() {
        return conditionIdentifier;
    }

    public void setConditionIdentifier(String conditionIdentifier) {
        this.conditionIdentifier = conditionIdentifier;
    }

    public Expression getRun() {
        return run;
    }

    public void setRun(Expression run) {
        this.run = run;
    }

    public Optional<Expression> getElseBlock() {
        return Optional.ofNullable(elseBlock);
    }

    public void setElseBlock(Expression elseBlock) {
        this.elseBlock = elseBlock;
    }
}
