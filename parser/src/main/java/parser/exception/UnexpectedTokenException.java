package parser.exception;

import tokens.TokenType;

public class UnexpectedTokenException extends RuntimeException {
    public UnexpectedTokenException(TokenType expected, TokenType got) {
        super("Unexpected token: Expected " + expected.name() + " but got " + got.name());
    }

    public UnexpectedTokenException(TokenType expected) {
        super("Unexpected token: Expected " + expected.name() + " but got nothing!");
    }
}
