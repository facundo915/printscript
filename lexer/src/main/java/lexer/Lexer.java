package lexer;

import line.Line;
import tokens.Token;

import java.util.stream.Stream;

public interface Lexer {
    Stream<Token> lex(Stream<Line> lines);
}
