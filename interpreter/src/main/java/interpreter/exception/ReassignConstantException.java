package interpreter.exception;

public class ReassignConstantException extends RuntimeException {
    public ReassignConstantException(){
        super("Tried to reassign constant variable");
    }
}
