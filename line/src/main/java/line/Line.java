package line;

public class Line {
    private String contents;
    private Integer line;
    private String file;

    public Line(String line, int i, String absolutePath) {
        this.contents = line;
        this.line = i;
        this.file = absolutePath;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
