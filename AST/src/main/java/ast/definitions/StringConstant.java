package ast.definitions;

import java.util.Map;

public class StringConstant implements Initializer {
    public StringConstant(String content) {
        value = content;
    }

    private String value;

    public String getValue(Map<String, Variable> context) {
        return value;
    }

    @Override
    public Type getType(Map<String, Variable> context) {
        return Type.STRING;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
