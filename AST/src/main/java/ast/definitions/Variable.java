package ast.definitions;

public class Variable {
    private Type type;
    private Boolean constant;
    private Object value;

    public Variable(Type type, Boolean constant, Object value) {
        this.type = type;
        this.constant = constant;
        this.value = value;
    }
    public Variable(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Boolean getConstant() {
        return constant;
    }

    public void setConstant(Boolean constant) {
        this.constant = constant;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
