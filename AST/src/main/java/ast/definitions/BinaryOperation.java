package ast.definitions;

import ast.exception.InvalidOperationException;

import java.util.Map;

public class BinaryOperation implements Initializer {
    private Initializer left;
    private Initializer right;
    private Operation operand;

    public Initializer getLeft() {
        return left;
    }

    public void setLeft(Initializer left) {
        this.left = left;
    }

    public Initializer getRight() {
        return right;
    }

    public void setRight(Initializer right) {
        this.right = right;
    }

    public Operation getOperand() {
        return operand;
    }

    public void setOperand(Operation operand) {
        this.operand = operand;
    }

    @Override
    public Object getValue(Map<String, Variable> context) {
        if (operand == Operation.ADDITION && left.getType(context) == Type.STRING || right.getType(context) == Type.STRING){
            String l = left.getValue(context).toString();
            String r = right.getValue(context).toString();
            return l + r;
        } else if (left.getType(context) == Type.NUMBER && right.getType(context) == Type.NUMBER){
            Double l = (double) left.getValue(context);
            Double r = (double) right.getValue(context);
            return operand.doubleFunction.apply(l, r);
        }
        System.out.println(left.getValue(context).toString() + right.getValue(context));
        throw new InvalidOperationException("Can't do operation " + operand.name() + " on types " + left.getType(context) + " and " + right.getType(context));
    }

    @Override
    public Type getType(Map<String, Variable> context) {
        if (left.getType(context) == Type.STRING || right.getType(context) == Type.STRING)
            return Type.STRING;
        if (operand == Operation.GREATER || operand == Operation.GREATER_OR_EQUAL || operand == Operation.LESS_OR_EQUAL || operand == Operation.LESS_THAN || operand == Operation.EQUALS) {
            return Type.BOOLEAN;
        }
        return Type.NUMBER;
    }
}
