package ast.definitions;

import java.util.Map;

public class VariableConstant implements Initializer{
    String identifier;

    public VariableConstant(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public Object getValue(Map<String, Variable> context) {
        return context.get(identifier).getValue();
    }

    @Override
    public Type getType(Map<String, Variable> context) {
        return context.get(identifier).getType();
    }
}
