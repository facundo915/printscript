package interpreter.impl;

import ast.definitions.Initializer;
import ast.definitions.Variable;
import ast.operations.Declaration;
import interpreter.exception.MismatchedTypeException;
import interpreter.exception.ReassignConstantException;
import interpreter.exception.UninitializedConstantExpression;
import interpreter.exception.UninitializedVariableException;

import java.util.HashMap;
import java.util.Map;

public class Context {
    private Map<String, Variable> values = new HashMap<>();

    public void declare(Declaration declaration) {
        if(declaration.getInitializer().isPresent()) {
            if(declaration.getType() != declaration.getInitializer().get().getType(values))
                throw new MismatchedTypeException(declaration.getType(),declaration.getInitializer().get().getType(values));
            values.put(declaration.getIdentifier(), new Variable(declaration.getType(),declaration.isConstant(),declaration.getInitializer().get().getValue(values)));
        }else{
            if(declaration.isConstant())
                throw new UninitializedConstantExpression();
        }
    }

    Map<String, Variable> getContextAsMap() {
        return values;
    }

    public Variable get(String identifier) {
        if(values.containsKey(identifier))
            return values.get(identifier);
        throw new UninitializedVariableException();
    }

    public void assign(String identifier, Initializer value) {
        Variable current = values.get(identifier);
        if(current.getType() != value.getType(values))
            throw new MismatchedTypeException(current.getType(),value.getType(values));
        if(current.getConstant()){
            throw new ReassignConstantException();
        }
        current.setValue(value.getValue(getContextAsMap()));
    }
}
