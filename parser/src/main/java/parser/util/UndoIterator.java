package parser.util;

import java.util.Iterator;
import java.util.Stack;

public class UndoIterator<T> implements Iterator<T>{
    private Stack<T> prev;
    private Stack<T> next;
    private Iterator<T> wrapee;

    public UndoIterator(Iterator<T> wrapee) {
        this.wrapee = wrapee;
        prev = new Stack<>();
        next = new Stack<>();
    }


    @Override
    public boolean hasNext() {
        return !next.isEmpty() || wrapee.hasNext();
    }

    @Override
    public T next() {
        if (next.isEmpty()) {
            T n = wrapee.next();
            prev.push(n);
            return n;
        }
        return next.pop();
    }

    public void undo(){
        next.push(prev.pop());
    }
}
